# Ekmek ve Gül Mobil Uygulama

[Click here for English](README.en.md)

Kadınların kadınlar için yaptığı haber sitesi [Ekmek ve Gül](https://ekmekvegul.net)'ün mobil uygulamasıdır.

Uygulamada bir hata bulduysanız Issues kısmından bildirebilirsiniz.

## Destek Olun

Halihazırda bir yazılımcıysanız, projeye destek olabilirsiniz. Uygulama, Ionic 3 ile yapılmıştır.

Kendi bilgisayarınızda çalıştırmak için `nodejs` kurulu olmalıdır.

#### Projenin kurulumu

Projeyi kopyalayalım.

`git clone git@gitlab.com:ekmek-ve-gul/ekmek-ve-gul-mobile.git`

`cd ekmek-ve-gul-mobile`

Gerekli javascript paketlerini yükleyelim:

`npm install`

Ve çalıştıralım!

`ionic serve`

Uygulama, tarayıcınızda açılacak. Responsive design modunu (CTRL + SHIFT + m) kullanarak, telefon boyutunda görebilirsiniz.
