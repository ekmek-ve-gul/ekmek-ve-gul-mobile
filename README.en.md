# Ekmek ve Gül Mobile App

[Türkçe için buraya tıklayın](README.md)

Hey, it's the mobile application for news website for women, from women: [Ekmek ve Gül](https://ekmekvegul.net).

Application is based on Ionic 3.

You can check the issues and send a merge request if you fix any of them. Also you can open new issues if you see any bug or want to see a feature.
