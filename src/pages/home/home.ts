import { Component, ViewChild } from '@angular/core';
import { LoadingController, NavController, Slides } from 'ionic-angular';
import { ContentService } from '../../app/commons';
import { ContentPage } from '../content/content';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  preserveWhitespaces: false,
})
export class HomePage {
  @ViewChild(Slides) headlineSlides: Slides;

  public headline: any[] = [];
  public categories: any[] = [];

  constructor(
    public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private contentService: ContentService
  ) { }

  ionViewDidLoad() {
    const spinner = this.loadingCtrl.create({ content: 'Yükleniyor...' });
    spinner.present();

    const headlinePromise = this.contentService.getHeadline()
      .then((response) => {
        this.headline = response;
        return true;
      })
      .catch((e) => {
        console.log(e);
        return false;
      });

    const categoryPromise = this.contentService.getLandingByCategories()
      .then((response) => {
        this.categories = response;
        return true;
      })
      .catch(() => {
        return false;
      });

    Promise.all([headlinePromise, categoryPromise])
      .then((response) => {
        spinner.dismiss();
      })
      .catch(() => spinner.dismiss());
  }

  public handleContentClick(content) {
    this.navCtrl.push(ContentPage, {
      content: content,
    });
  }
}
