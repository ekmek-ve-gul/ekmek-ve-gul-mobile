import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { CategoryService } from '../../app/commons';
import { ContentPage } from '../content/content';

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
  preserveWhitespaces: false,
})
export class CategoryPage {
  public categoryId: number;
  public category: any;
  public contentList = [];
  public page = 1;
  public loadingBottom = false;
  public loadingBottomDisabled = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private categoryService: CategoryService,
    private loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {
    const spinner = this.loadingCtrl.create({
      // content: "Please wait...",
    });

    spinner.present();
    this.categoryId = this.navParams.get('categoryId');

    this.categoryService.getCategory(this.categoryId)
      .then((response) => {
        this.category = response.category;
        if (response.contents) {
          for (const content of response.contents) {
            content.mainImage = this.getContentMainMedia(content);
            this.contentList.push(content);
          }
        }
        spinner.dismiss();

        if (response.lastChecked + this.categoryService.categoryCacheTtl * 1000 < (new Date).getTime()) {
          return this.loadTop();
        }
      })
      .catch(() => {
        spinner.dismiss();
      });
  }

  public loadBottom() {
    if (this.loadingBottom) {
      return;
    }
    this.loadingBottom = true;

    return this.categoryService.getCategoryBefore(this.categoryId)
      .then((response) => {
        for (const content of response) {
          content.mainImage = this.getContentMainMedia(content);
          this.contentList.push(content);
        }

        if (!response.length) {
          this.loadingBottomDisabled = true;
        }
      })
      .catch(() => { })
      .then(() => this.loadingBottom = false);
  }

  public loadTop(refresher?) {
    const spinner = this.loadingCtrl.create();
    if (!refresher) {
      spinner.present();
    }

    this.categoryService.getCategoryAfter(this.categoryId)
      .then((response) => {
        response = response.reverse();
        for (const content of response) {
          content.mainImage = this.getContentMainMedia(content);
          this.contentList.unshift(content);
        }
        if (refresher) {
          refresher.complete();
        } else {
          spinner.dismiss();
        }
      })
      .catch(() => {
        if (refresher) {
          refresher.cancel();
        } else {
          spinner.dismiss();
        }
      });
  }

  public getContentMainMedia(content) {
    let path;

    if (content.content_main_media) {
      path = content.content_main_media.url;
    } else if (content.medias && content.medias.length > 0) {
      path = content.medias[0].url;
    } else {
      return null;
    }

    return path;
  }

  public handleContentClick(content) {
    this.navCtrl.push(ContentPage, {
      content: content,
    });
  }
}
