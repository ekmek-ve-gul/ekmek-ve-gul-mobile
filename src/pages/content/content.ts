import { Component } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ContentService } from '../../app/commons';

/**
 * Generated class for the ContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html',
  preserveWhitespaces: false,
})
export class ContentPage {
  public content: any = {};
  public spinner: Loading;
  public videoHtml: SafeHtml = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private contentService: ContentService,
    private loadingCtrl: LoadingController,
    private shareService: SocialSharing,
    private sanitizer: DomSanitizer
  ) { }

  ionViewDidLoad() {
    this.spinner = this.loadingCtrl.create({
      content: 'Yükleniyor...',
    });

    if (this.navParams.get('contentSlug')) {
      this.loadContentFromSlug(this.navParams.get('contentSlug'));
    } else {
      this.content = this.navParams.get('content');

      if (this.navParams.get('related')) {
        this.loadContent(this.content.id);
      }
    }
  }

  public loadContent(contentId) {
    this.spinner.present();
    return this.contentService.getContent(contentId)
      .then((response) => {
        this.content = response;
      })
      .catch(() => { })
      .then(() => this.spinner.dismiss());
  }

  public loadContentFromSlug(contentSlug) {
    const spinner = this.loadingCtrl.create({
      content: 'Yükleniyor...',
    });
    spinner.present();

    this.contentService.getContentBySlug(contentSlug)
      .then((response) => {
        this.content = response;
      })
      .catch(() => { })
      .then(() => spinner.dismiss());
  }

  public getMainMedia() {
    return this.content.content_main_media.url;
  }

  public share() {
    const spinner = this.loadingCtrl.create({
      content: 'Yükleniyor...',
    });
    spinner.present();

    this.shareService.share(
      this.content.title, // message
      this.content.title, // subject
      this.getMainMedia(), // media
      this.content.short_url, // link to page
    )
      .then(() => spinner.dismiss())
      .catch(() => spinner.dismiss());
  }

  public handleContentClick(content) {
    this.navCtrl.push(ContentPage, {
      content: content,
      related: true,
    });
  }

  public handleInlineLinks($event) {
    if (!$event) {
      return;
    }

    if ($event.path.length > 0 && $event.path[0].tagName.toLowerCase() === 'a') {
      const href = $event.path[0].href;
      if (href.indexOf('://ekmekvegul.net') > -1) {
        const slugArray = href.split('ekmekvegul.net/')[1].split('/');
        if (slugArray.length === 2) {
          this.navCtrl.push(ContentPage, {
            contentSlug: slugArray[1],
          });
          return false;
        }
      }
    }

    return;
  }

  public handlePlayClick() {
    this.videoHtml = this.sanitizer.bypassSecurityTrustHtml(this.content.content_main_media.iframe_code);
  }
}
