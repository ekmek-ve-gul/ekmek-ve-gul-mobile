import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  preserveWhitespaces: false,
})
export class AboutPage {
  constructor() { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
}
