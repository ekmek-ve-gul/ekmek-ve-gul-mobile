import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Deeplinks } from '@ionic-native/deeplinks';
import { ImgCacheService } from 'ng-imgcache';

import { AboutPage } from '../pages/about/about';
import { CategoryPage } from '../pages/category/category';
import { ContentPage } from '../pages/content/content';
import { HomePage } from '../pages/home/home';
import { HttpService } from './commons';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('content') navContent: NavController;

  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any, params?: any }>;

  constructor(
    private deeplinks: Deeplinks,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private http: HttpService,
    private imgCacheService: ImgCacheService
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'ANA SAYFA', component: HomePage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();

      if (this.platform.is('cordova')) {
        this.imgCacheService.init({});
      }

      this.deeplinks.route({
        '/:categorySlug': CategoryPage,
        '/:categorySlug/:contentSlug': ContentPage,
      }).subscribe(
        (match) => {
          this.nav.setRoot(match.$route, match.$args);
          console.log(match);
        },
        (nomatch) => {
          console.log(nomatch);
        });

      this.http.get('categories', null, { cache: true, ttl: 60 * 60 * 24 * 10 })
        .then((response) => {
          for (const category of response) {
            this.pages.push({
              title: category.title,
              component: CategoryPage,
              params: {
                categoryId: category.id,
              },
            });
          }
          this.splashScreen.hide();
        })
        .catch(() => this.splashScreen.hide())
        .then(() => {
          this.pages.push({
            title: 'HAKKINDA',
            component: AboutPage,
          });
        });
    });
  }

  openPage(page, params = {}) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, params);
  }
}
