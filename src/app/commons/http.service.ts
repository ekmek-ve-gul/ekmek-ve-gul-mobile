import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { StorageService } from './storage.service';

// import { environment } from '../../environments/environment';

@Injectable()
export class HttpService {

  // private apiUrl: string = environment.apiUrl;
  private apiUrl: string = 'https://ekmekvegul.net/api/';

  constructor(
    private http: HttpClient,
    private storage: StorageService
  ) { }

  public get(
    url: string,
    params?: Object,
    config?: { httpConfig?: any, cache?: boolean, ttl?: number }
  ): Promise<any> {
    config = config || {
      httpConfig: {},
      cache: false,
    };
    if (!config.ttl) {
      config.ttl = 60 * 60 * 24;
    }

    const httpConfig = config.httpConfig ? config.httpConfig : {};

    if (params) {
      httpConfig.params = params;
    }

    // without caching
    if (!config.cache) {
      return new Promise((resolve, reject) => {
        this.http.get(this.buildApiUrl(url), httpConfig).subscribe(
          (successResponse: any) => {
            if (successResponse.data) {
              resolve(successResponse.data);
            } else {
              throw new Error('NOT_VALID_RESPONSE');
            }
          },
          (error: HttpErrorResponse) => {
            reject(error);
          }
        );
      });
    }

    // with caching
    return this.storage.getCacheItem(url)
      .then((result) => {
        return result;
      })
      .catch((a) => {
        return new Promise((resolve, reject) => {
          this.http.get(this.buildApiUrl(url), httpConfig).subscribe(
            (successResponse: any) => {
              if (successResponse.data) {
                if (config.cache) {
                  this.storage.cacheItem(url, successResponse.data, config.ttl);
                }
                resolve(successResponse.data);
              } else {
                throw new Error('NOT_VALID_RESPONSE');
              }
            },
            (error: HttpErrorResponse) => {
              reject(error);
            }
          );
        });
      });
  }

  public post(url: string, params?: Object, config?: { params?: any }): Promise<any> {
    config = config || {};

    const httpPromise = new Promise((resolve, reject) => {
      this.http.post(this.buildApiUrl(url), params).subscribe(
        (successResponse) => {
          resolve(successResponse);
        },
        (error: HttpErrorResponse) => {
          reject(error);
        }
      );
    });

    return httpPromise;
  }

  private buildApiUrl(url: string) {
    if (url.substring(0, 7) === 'http://' || url.substring(0, 8) === 'https://') {
      return url;
    }

    return this.apiUrl + url;
  }

}
