import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageService {
  constructor(
    private storage: Storage
  ) { }

  public cacheItem(key, data, ttl = 60 * 60 * 24): Promise<any> {
    const cacheData = {
      expires: new Date().getTime() + (ttl * 1000),
      data: JSON.stringify(data),
    };

    return this.storage.set('cache-' + key, cacheData);
  }

  public getCacheItem(key) {
    return this.storage.get('cache-' + key)
      .then((result) => {
        if (result && result.data && result.expires > new Date().getTime()) {
          return JSON.parse(result.data);
        } else {
          return Promise.reject('NOT_FOUND');
        }
      })
  }

  public setItem(key, data): Promise<any> {
    const cacheData = {
      data: JSON.stringify(data),
    };

    return this.storage.set(key, cacheData);
  }

  public getItem(key): Promise<any> {
    return this.storage.get(key)
      .then((result) => {
        if (result && result.data) {
          return JSON.parse(result.data);
        } else {
          return Promise.reject('NOT_FOUND');
        }
      });
  }
}
