import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { StorageService } from './storage.service';

@Injectable()
export class CategoryService {
  private categoryDB;
  private dbVersion = 1;
  public categoryCacheTtl = 60 * 60; // minutes
  public categoryCacheLongTtl = 60 * 60 * 24 * 7; // minutes
  public ready: Promise<any>;

  constructor(
    private http: HttpService,
    private storage: StorageService
  ) {
    this.init();
  }

  private init() {
    this.ready = this.storage.getItem('categories')
      .then((response) => {
        if (!response || response.dbVersion !== this.dbVersion) {
          this.categoryDB = {
            dbVersion: this.dbVersion,
          };
        } else {
          this.categoryDB = response;
        }
        // this.categoryDB = {}; // debug: resets the storage
      })
      .catch((response) => {
        this.categoryDB = {
          dbVersion: this.dbVersion,
        };
      })
      .then(() => {
        setInterval(() => this.save(), 10000);
      });

    return this.ready;
  }

  public getCategory(categoryId): Promise<any> {
    return this.ready.then(() => {
      if (this.categoryDB['category-' + categoryId]) {
        const lastChecked = this.categoryDB['category-' + categoryId].lastChecked;
        if (lastChecked && lastChecked + this.categoryCacheLongTtl * 1000 > (new Date).getTime()) {
          return Promise.resolve(this.categoryDB['category-' + categoryId]);
        }
      }

      return this.http.get('categories/' + categoryId)
        .then((response) => {
          if (response) {
            this.categoryDB['category-' + categoryId] = this.categoryDB['category-' + categoryId] || {};
            Object.assign(this.categoryDB['category-' + categoryId], {
              category: {
                id: response.id,
                title: response.title,
                description: response.description,
                color_code: response.color_code,
              },
              contents: [],
              publishedAtFirst: null,
              publishedAtLast: null,
              lastChecked: (new Date()).getTime(),
            });
            this.importContents(categoryId, response.contents);
            return this.categoryDB['category-' + categoryId];
          }
        })
        .catch((e) => {
          console.log(e);
        });
    });
  }

  public getCategoryAfter(categoryId) {
    if (!this.categoryDB['category-' + categoryId]) {
      return this.getCategory(categoryId)
        .then((response) => {
          return response.contents;
        });
    }

    const publishedAtAfter = this.categoryDB['category-' + categoryId].publishedAtFirst;

    return this.http.get('categories/' + categoryId, { publishedAtAfter: publishedAtAfter })
      .then((response) => {
        this.importContents(categoryId, response.contents, 'after')
        return response.contents;
      });
  }

  public getCategoryBefore(categoryId) {
    if (!this.categoryDB['category-' + categoryId]) {
      return this.getCategory(categoryId)
        .then((response) => {
          return response.contents;
        });
    }

    const publishedAtBefore = this.categoryDB['category-' + categoryId].publishedAtLast;

    return this.http.get('categories/' + categoryId, { publishedAtBefore: publishedAtBefore })
      .then((response) => {
        this.importContents(categoryId, response.contents, 'before')
        return response.contents;
      });
  }

  private importContents(categoryId: number, contents: any[], whereToAdd = 'after') {
    let method = 'push';
    if (whereToAdd === 'after') {
      method = 'unshift';
      contents.reverse();
    }

    for (const content of contents) {
      this.categoryDB['category-' + categoryId].contents[method](content);

      if (content.published_at > this.categoryDB['category-' + categoryId].publishedAtFirst || this.categoryDB['category-' + categoryId].publishedAtFirst === null) {
        this.categoryDB['category-' + categoryId].publishedAtFirst = content.published_at;
      }

      if (content.published_at < this.categoryDB['category-' + categoryId].publishedAtLast || this.categoryDB['category-' + categoryId].publishedAtLast === null) {
        this.categoryDB['category-' + categoryId].publishedAtLast = content.published_at;
      }
    }

    if (whereToAdd === 'after') {
      contents.reverse();
    }
  }

  private save() {
    this.storage.setItem('categories', this.categoryDB);
  }
}
