import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { CategoryService } from './category.service';
import { ContentService } from './content.service';
import { HttpService } from './http.service';
import { StorageService } from './storage.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
  ],
  declarations: [
  ],
  providers: [
    CategoryService,
    ContentService,
    HttpService,
    StorageService,
  ],
})
export class CommonsModule { }
