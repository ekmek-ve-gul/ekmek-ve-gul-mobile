import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable()
export class ContentService {
  public ready: Promise<any>;

  constructor(
    private http: HttpService
  ) { }

  public getContent(contentId: number) {
    return this.http.get('contents/' + contentId, null, { cache: true, ttl: 60 * 60 * 3 })
      .then((response) => {
        if (response) {
          return response;
        } else {
          throw new Error('NOT_FOUND');
        }
      });
  }

  public getContentBySlug(contentSlug: string) {
    return this.http.get('contents/' + contentSlug, null, { cache: true, ttl: 60 * 60 * 3 })
      .then((response) => {
        if (response) {
          return response;
        } else {
          throw new Error('NOT_FOUND');
        }
      });
  }

  public getHeadline(): Promise<any[]> {
    return this.http.get('contents/get-headline', null, { cache: true, ttl: 60 * 10 })
      .then((response) => {
        if (response) {
          return response;
        } else {
          throw new Error('SERVER_ERROR');
        }
      });
  }

  public getLandingByCategories(): Promise<any[]> {
    return this.http.get('contents/get-landing', null, { cache: true, ttl: 60 * 10 })
      .then((response) => {
        if (response) {
          return response;
        } else {
          throw new Error('SERVER_ERROR');
        }
      });
  }
}
