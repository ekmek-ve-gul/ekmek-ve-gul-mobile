import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { ImgCacheModule } from 'ng-imgcache';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Deeplinks } from '@ionic-native/deeplinks';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { CommonsModule } from './commons/commons.module';
import { CustomErrorHandler } from './commons';
import { HomePage } from '../pages/home/home';
import { AboutPageModule } from '../pages/about/about.module';
import { CategoryPageModule } from '../pages/category/category.module';
import { ContentPageModule } from '../pages/content/content.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    AboutPageModule,
    BrowserModule,
    CategoryPageModule,
    CommonsModule,
    ContentPageModule,
    ImgCacheModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    Deeplinks,
    StatusBar,
    SocialSharing,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: ErrorHandler, useClass: CustomErrorHandler },
  ]
})
export class AppModule { }
